react-postcss-components
=====================

Dev environment for creating Reactjs components with webpack and postcss.

### Usage

```
git clone git@gitlab.com:alavi09/react-postcss-components.git
npm install
npm run dev
open http://localhost:8080/webpack-dev-server/
```

### Dependencies

* React
* Webpack
* Postcss(precss and nextcss included)
* Babel
* Eslint