import React from 'react';
import ReactDOM from 'react-dom';

/* dropdown component */

//import './sampleUsageDropdown'

/* Navigation component */

//import './sampleNavUsage'

/* Hero */

//import HomePage from './HomePage'

import {Router, browserHistory} from 'react-router'
import HomePage from './HomePage'
import routes from './routes'

ReactDOM.render(
  <Router history={browserHistory} routes={routes} />,
  document.getElementById('root')
);