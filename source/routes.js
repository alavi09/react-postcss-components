
import React, {Component} from 'react';
import { Route, IndexRoute, Link } from 'react-router';
import HomePage from './HomePage'
import App from './components/App'

// const Home = () => <h1>Home Page</h1>
// const About = () => <h1>About Page</h1>

// class App extends Component {
//     render() {
//         return (
//             <div>
//                 {this.props.children}
//             </div>
//         );
//     }
// }

// class Home extends Component {
//     render() {
//         return (
//             <div>
//             <h1>Home Page</h1>
//             <Link to="about">Learn more</Link>
//             </div>
//         );
//     }
// }

class About extends Component {
    render() {
        return (
            <h1>About Page</h1>
        );
    }
}

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="about" component={About} />
  </Route>
);