/*  Dropdown component 
    @pram {array} Array of objects with information to be shown inside dropdown. Keys {value,url}
    @pram {object} Style object. Keys {dropdown,list}
    @example    
    <Dropdown {..._styles} buttonText={ 'Dropdown' }>
        <List {..._styles} listdata={_data} />
    </Dropdown>

*/

import React, {Component} from 'react';
import List from './List'

class Dropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // initial state of dropdown menu
            expanded: false
        };
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        // on click, if menu is closed(expanded is set to false)..
        if (this.state.expanded === false) {
            this.setState({ menuStateStyle: this.props.dropdown.menuOpenStyle, expanded: !this.state.expanded });
        }
        // else if menu is open..
        else {
            this.setState({ menuStateStyle: null, expanded: !this.state.expanded });
        }
    }
    render() {
        return (
            <div className={this.props.parent}>
                <button className={this.props.dropdown.buttonStyle} type="button" onClick={this.handleClick} aria-expanded={this.state.expanded}>{this.props.buttonText}
                </button>
                <div className={this.state.menuStateStyle}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Dropdown;