import React, {Component} from 'react';

class LinkList extends Component {
    render() {
        return (
            <li className={this.props.styles}><a href={this.props.data.url}>{this.props.data.value}</a></li>
        );
    }
}

export default LinkList;