/*  Navigation Component
    @param {string} brandText, title of page or site.
    @pram {array} Data array with keys; [{innerText, url}]
    @pram {styles} Css styles 
    @example
        <Navigation 
            brandText={ brandTitle }
            navMenuData={_data}
            {..._styles}
        />
*/

import React, {Component} from 'react';

//Generate links as routes using react router
import {Link}  from 'react-router'

export default class Navigation extends Component {
    constructor(props) {
        super(props)
        this.showMenuItems = this.showMenuItems.bind(this);
    }
    //generate navigation menu items using react-router Link component by returning a new array via map
    showMenuItems() {        
        return this.props.navMenuData.map((obj, index) => {
            return <Link className={this.props.nav_bar_item} to={obj.url}
                key={index}>{obj.innerText}</Link>
        });
    }
    render() {
        return (
            <header className={this.props.header}>
                <div className={this.props.header_row}>
                    <span className={this.props.brand_title}>
                        {this.props.brandText}
                    </span>
                    <nav className={this.props.nav_container}>
                        {this.showMenuItems()}
                    </nav>
                </div>
            </header>
        );
    }
}


