import React, { Component, PropTypes } from 'react';
import Navigation from './Navigation'
import Footer from './Footer'
import "../main.css"

const _styles = {
    nav: {
        nav_bar_item: 'nav-bar--item',
        nav_container: 'nav-container',
        header: 'flex-header',
        header_row: 'flex-header--row',
        brand_title: 'brand-title'
    }
}
const brandTitle = 'Site!!'

const _data = [{ innerText: 'About', url: 'about' }, { innerText: 'Link', url: '#' }, { innerText: 'Three', url: '#' }]

class App extends Component {
    render() {
        return (
            <div>
                <Navigation
                    brandText={ brandTitle }
                    navMenuData={_data}
                    {..._styles.nav}
                    />
                {this.props.children}
                <Footer>
                    <small> Site details</small>
                </Footer>
            </div>
        );
    }
}

App.propTypes = {
  children: PropTypes.object.isRequired
};

export default App;
