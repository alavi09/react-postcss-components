import React, {Component} from 'react';

class Footer extends Component {
    render() {
        return (
            <footer>
            <hr />
                <div className={''}>
                    {this.props.children}
                </div>
            </footer>
        );
    }
}

export default Footer;