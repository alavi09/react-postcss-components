import React, { Component } from 'react';
import Listitem from './Listitem'
import Linklist from './Linklist'
import './App.css';

export default class List extends Component {
    constructor(props) {
        super(props)
        this.generateListItems = this.generateListItems.bind(this);
    }
    generateListItems() {
        return this.props.listdata.map((item, index) => {
            // if a url is provided in listdata, create a link
            return item.url ? 
            <Linklist styles={this.props.list.itemStyle} data={ item } key={index}></Linklist>
            // else generate normal listitems 
            : <Listitem styles={this.props.list.itemStyle} data={ item } key={index}></Listitem>
        })
    }
    render() {
        return (
            <ul className={this.props.list.listStyle}>
                {this.generateListItems()}
            </ul>
        );
    }
}