import React, {Component} from 'react';

class List extends Component {
    render() {
        return (
            <li className={this.props.styles}>{this.props.data.value}</li>
        );
    }
}

export default List;