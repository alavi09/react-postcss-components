/*  Hero Component
    @param 

*/

import React, {Component} from 'react';

class Hero extends Component {
    render() {
        return (
            <section>
                <div className={this.props.heroMain}>
                    <main className={this.props.heroContent}>
                        {this.props.children}
                    </main>
                    <img className={this.props.heroMedia} src={this.props.heroImage} />
                </div>
            </section>
            );
            }
}

export default Hero;