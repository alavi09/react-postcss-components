import React from 'react';
import ReactDOM from 'react-dom';
import Navigation from './components/Navigation'
import './main.css'

const _styles = {
        nav_bar_item: 'nav-bar--item',
        nav_container: 'nav-container',
        header: 'flex-header',
        header_row: 'flex-header--row',
        brand_title: 'brand-title'
}

const brandTitle = 'Test!!'

const _data = [{ innerText: 'Link', url: '#' }, { innerText: 'Link', url: '#' }, { innerText: 'Three', url: '#' }]

ReactDOM.render(
    <Navigation 
        brandText={ brandTitle }
        navMenuData={_data}
        {..._styles}
    />,
    document.getElementById('root')
);