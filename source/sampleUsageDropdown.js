import React from 'react';
import ReactDOM from 'react-dom';
import Dropdown from './components/Dropdown'
import List from './components/List'
import './main.css'

// component data for generating dropdown values and link urls
const _mydata = [{ value: 'First', url: 'link.html' }, { value: 'Two', url: 'link.html' }, { value: 'No Link' }]

// styles imported from css fail
const _styles = {
  dropdown: {
    buttonStyle: 'btn',
    menuOpenStyle: 'open'
  },
  list: { 
    listStyle: 'dropdown_menu', 
    itemStyle: 'dropdown_item' 
  }
}

export default ReactDOM.render(
  <Dropdown {..._styles} buttonText={ 'Dropdown' }>
    <List {..._styles} listdata={_mydata} />
  </Dropdown>,
  document.getElementById('root')
);
