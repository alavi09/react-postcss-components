import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Hero from './components/Hero'


const _styles = {
    hero: { heroMain: 'hero', heroContent: 'hero_content', heroMedia: 'hero_media' }
}

const imgUrl = 'https://dl.dropboxusercontent.com/u/7094141/Light_cloud1300.jpg'

class HomePage extends Component {
    render() {
        return (
            <Hero
                {..._styles.hero}
                heroImage={ imgUrl }>
                <h2>I am a hero</h2>
                <p> Ea magna cillum do pariatur.Aliqua ullamco minim proident dolor veniam incididunt est cillum.Duis dolor officia qui labore nulla elit non adipisicing cillum officia irure.Sit ea minim duis excepteur dolore elit in elit qui ad ullamco.Velit occaecat exercitation irure cupidatat mollit.Tempor officia labore minim cillum.Cupidatat fugiat quis voluptate ut veniam ea aliqua consequat ipsum magna veniam ex enim.</p>
                <button className="btn">Start</button>
            </Hero>
        );
    }
}
export default HomePage;