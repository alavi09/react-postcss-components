import Webpack from 'webpack'
import path from 'path'
import precss from 'precss'
import postcssImport from 'postcss-import'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import cssnext from 'postcss-cssnext'

const browserList = [
    'Android >= 2.3',
    'BlackBerry >= 7',
    'Chrome >= 9',
    'Firefox >= 4',
    'Explorer >= 8',
    'iOS >= 5',
    'Opera >= 11',
    'Safari >= 5',
    'OperaMobile >= 11',
    'OperaMini >= 6',
    'ChromeAndroid >= 9',
    'FirefoxAndroid >= 4',
    'ExplorerMobile >= 9'
]

export default {

    entry: [
        'webpack/hot/dev-server',
        'webpack-dev-server/client?http://localhost:3000',
        'webpack/hot/only-dev-server', 
        path.resolve(__dirname, 'source/index.js')
    ],
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js',
        publicPath: '/public/'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loaders: ['react-hot', 'babel'],
            include: path.join(__dirname, 'source')
        }, {
                test: /\.css$/,
                loaders: [
                    'style-loader',
                    ExtractTextPlugin.extract('css-loader'),
                    'postcss-loader',
                ]
            }]
    },
    plugins: [
        new ExtractTextPlugin('style.css', {
            allChunks: true
        }),
        new Webpack.HotModuleReplacementPlugin()
    ],
    postcss: (webpack) => {
        return [
            postcssImport({
                addDependencyTo: webpack
            }), precss, cssnext({
                browsers: browserList
            })
        ]
    },
    devtool: '#inline-source-map'
}